terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

variable "sku_name" {
  type = string
}

variable "max_connections" {
  type     = number
  nullable = true
  default  = null
}

locals {
  # https://learn.microsoft.com/en-us/azure/postgresql/flexible-server/concepts-limits
  pg_max_connections_map = {
    "B_Standard_B1ms"       = 50
    "B_Standard_B2s"        = 429
    "B_Standard_B2ms"       = 859
    "B_Standard_B4ms"       = 1718
    "B_Standard_B8ms"       = 3437
    "B_Standard_B12ms"      = 5000
    "B_Standard_B16ms"      = 5000
    "B_Standard_B20ms"      = 5000
    "GP_Standard_D2s_v3"    = 859
    "GP_Standard_D2ds_v4"   = 859
    "GP_Standard_D2ds_v5"   = 859
    "GP_Standard_D2ads_v5"  = 859
    "GP_Standard_D4s_v3"    = 1718
    "GP_Standard_D4ds_v4"   = 1718
    "GP_Standard_D4ds_v5"   = 1718
    "GP_Standard_D4ads_v5"  = 1718
    "GP_Standard_D8s_v3"    = 3437
    "GP_Standard_D8ds_V4"   = 3437
    "GP_Standard_D8ds_v5"   = 3437
    "GP_Standard_D8ads_v5"  = 3437
    "GP_Standard_D16s_v3"   = 5000
    "GP_Standard_D16ds_v4"  = 5000
    "GP_Standard_D16ds_v5"  = 5000
    "GP_Standard_D16ads_v5" = 5000
    "GP_Standard_D32s_v3"   = 5000
    "GP_Standard_D32ds_v4"  = 5000
    "GP_Standard_D32ds_v5"  = 5000
    "GP_Standard_D32ads_v5" = 5000
    "GP_Standard_D48s_v3"   = 5000
    "GP_Standard_D48ds_v4"  = 5000
    "GP_Standard_D48ds_v5"  = 5000
    "GP_Standard_D48ads_v5" = 5000
    "GP_Standard_D64s_v3"   = 5000
    "GP_Standard_D64ds_v4"  = 5000
    "GP_Standard_D64ds_v5"  = 5000
    "GP_Standard_D64ads_v5" = 5000
    "GP_Standard_D96ds_v5"  = 5000
    "GP_Standard_D96ads_v5" = 5000
    "MO_Standard_E2s_v3"    = 1718
    "MO_Standard_E2ds_v4"   = 1718
    "MO_Standard_E2ds_v5"   = 1718
    "MO_Standard_E2ads_v5"  = 1718
    "MO_Standard_E4s_v3"    = 3437
    "MO_Standard_E4ds_v4"   = 3437
    "MO_Standard_E4ds_v5"   = 3437
    "MO_Standard_E4ads_v5"  = 3437
    "MO_Standard_E8s_v3"    = 5000
    "MO_Standard_E8ds_v4"   = 5000
    "MO_Standard_E8ds_v5"   = 5000
    "MO_Standard_E8ads_v5"  = 5000
    "MO_Standard_E16s_v3"   = 5000
    "MO_Standard_E16ds_v4"  = 5000
    "MO_Standard_E16ds_v5"  = 5000
    "MO_Standard_E16ads_v5" = 5000
    "MO_Standard_E20ds_v4"  = 5000
    "MO_Standard_E20ds_v5"  = 5000
    "MO_Standard_E20ads_v5" = 5000
    "MO_Standard_E32s_v3"   = 5000
    "MO_Standard_E32ds_v4"  = 5000
    "MO_Standard_E32ds_v5"  = 5000
    "MO_Standard_E32ads_v5" = 5000
    "MO_Standard_E48s_v3"   = 5000
    "MO_Standard_E48ds_v4"  = 5000
    "MO_Standard_E48ds_v5"  = 5000
    "MO_Standard_E48ads_v5" = 5000
    "MO_Standard_E64s_v3"   = 5000
    "MO_Standard_E64ds_v4"  = 5000
    "MO_Standard_E64ds_v5"  = 5000
    "MO_Standard_E64ads_v5" = 5000
    "MO_Standard_E96ds_v5"  = 5000
    "MO_Standard_E96ads_v5" = 5000
  }
}

resource "azurerm_postgresql_flexible_server" "current" {
  # ...
  sku_name          = var.sku_name
}

# This server parameter is set on creation and never updated automatically. We have to sync it manually.
resource "azurerm_postgresql_flexible_server_configuration" "max_connections" {
  server_id = azurerm_postgresql_flexible_server.current.id
  name      = "max_connections"
  value     = var.max_connections == null ? local.pg_max_connections_map[var.sku_name] : var.max_connections
}
